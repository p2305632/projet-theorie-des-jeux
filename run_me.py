import sys
import calcul
import plot
import simulation
import calculModifs

def continuer():
    print("\nAppuyez sur la touche 'entrée' pour continuer", end='', flush=True)
    
    input()
    
    # Pour effacer le print("\nAppuyez sur la touche 'entrée' pour continuer", end='', flush=True)
    # Ces trois lignes peuvent être supprimées en cas de problème
    sys.stdout.write('\033[F')
    sys.stdout.write('\033[K')
    sys.stdout.flush()

def main():
    calcul.mp = 2
    calcul.m = 2 * calcul.mp + 1
    calcul.n = 20
    pierresJ1 = 5
    pierresJ2 = 4
    positionTroll = -1

    print("---------------------- Projet Théorie des Jeux ----------------------\n")

    print(f"NB: Si le programme réagit mal en appuyant sur la touche Entrée quand cela est proposé, vous pouvez supprimer les trois dernières lignes de la fonction run_me.continuer(). Elles peuvent poser des problèmes sur certaines consoles inhabituelles.\n")

    # Démo de la configuration
    print(f"Configuration:\n\tm' = {calcul.mp}\n\tm = {calcul.m}\n")

    continuer()

    # Démo de l'affichage d'une configuration
    print("- Affichage d'une configuration:")
    print(f"\tDans le fichier calcul.py, afficherConfig(({pierresJ1}, {pierresJ2}, {positionTroll})) produit le résultat suivant :\n\n\t", end='')
    calcul.afficherConfig((pierresJ1, pierresJ2, positionTroll))
    continuer()
    print("\n\tOn voit à gauche le joueur 1 et son nombre de pierres, et à droite le joueur 2 et son nombre de pierres. La position du troll est symbolisée par la lettre T.\n")
    
    continuer()

    # Démo de la valeur du jeu
    print("- Valeur d'un jeu:")
    print(f"\tDans le fichier calcul.py, la fonction calculeVS(x, y, t, memo) permet d'obtenir la valeur d'un jeu ainsi que la stratégie optimale du joueur 1.\n")
    print("\tAvec l'exemple du sujet, on obtient :")
    pierresJ1 = 5
    pierresJ2 = 4
    positionTroll = -1
    valeur, strategie = calcul.calculeVS(pierresJ1, pierresJ2, positionTroll)
    print(f"V({pierresJ1}, {pierresJ2}, {positionTroll}) =", valeur)
    print(f"Sopt({pierresJ1}, {pierresJ2}, {positionTroll}) =", strategie)
    
    continuer()

    # Démo de la construction et de l'afficahge du graphique du temps d'exécution
    nstart = 1
    nend = 20
    nstep = 1
    print("- Construction et affichage du graphe des temps d'exécution:")
    print(f"\tLa fonction plotTime(start, end, step) du fichier plot.py permet de construire le graphe du temps d'exécution de calculVS(n, n, -1) en fonction de n. On peut choisir la valeur de départ et de fin pour n, ainsi que le pas pour n. Le résultat produit par l'appel de plotTime({nstart}, {nend}, {nstep}) devrait s'ouvrir dans une fenêtre annexe.")
    continuer()
    print("\tConstruction du graphe... En attendant, en voici un commentaire: on a en abscisses la valeur de n, et en ordonnées le temps d'exécution de calculVS. On observe un temps d'exécution polynomial.")
    print("\tQuand le graphique s'affichera, fermer la fenêtre pour continuer ce programme de démonstration.")
    plot.plotTime(nstart, nend, nstep)

    continuer()

    # Démo d'un affrontement entre la stratégie optimale et la stratégie aléatoire
    nstart = 1
    nend = 20
    nstep = 1
    nbParties = 100000
    heuristiqueJ2 = simulation.StrategieAleatoire
    print(f"- Affrontement entre la stratégie optimale et une heuristique aléatoire:")
    print(f"\tLa fonction simuler(n, nbParties, heuristiqueJ2) du fichier simulation.py va simuler plusieurs parties entre la solution optimale et une heuristique au choix, par défaut, la stratégie aléatoire. Nous allons jouer {nbParties} parties pour n allant de {nstart} à {nend} avec un pas de {nstep}. Pour chaque valeur de n, nous calculerons v1 / v2, le nombre de victoires relatif de J1 sur J2 (afin de ne pas prendre en compte les matches nuls). Si cela prend trop de temps, vous pouvez diminuer la valeur de la variable nbParties dans run_me.py.")
    print("\tCalcul du graphe en cours...")
    print("\tQuand le graphique s'affichera, fermer la fenêtre pour continuer ce programme de démonstration.")
    print("\tOn observe une évolution logarithmique.")
    plot.plotOptiContreHeuristique(nstart, nend, nstep, nbParties, heuristiqueJ2)

    continuer()

    # Démo de la valeur et stratégie optimale d'un jeu dans les règles modifiées
    print(f"Nous avons modifié les règles pour le TP noté. A savoir, nous avons fait en sorte que J2 ne puisse que jouer un nombre impair de pierres. Voici les résultats sur la stratégie optimale sur la même configuration que pour les règles normales :")
    pierresJ1 = 5
    pierresJ2 = 4
    positionTroll = -1
    valeur, strategie = calculModifs.calculeVS(pierresJ1, pierresJ2, positionTroll)
    print(f"V({pierresJ1}, {pierresJ2}, {positionTroll}) =", valeur)
    print(f"Sopt({pierresJ1}, {pierresJ2}, {positionTroll}) =", strategie)

    continuer()

    print(f"On observe que alors que la valeur de cette configuration était négative intialement (donc en faveur de J2), elle est nulle maintenant. Les règles ont dont l'air d'être à l'avantage de J1.")

    continuer()

    # Démo d'un affrontement avec les modifications des règles 
    print(f"Voici les résultats d'un affrontement entre J1 et J2 avec les règles modifiées, où J1 joue la stratégie optimale adaptée aux règles modifiées, et J2 joue la stratégie aléatoire :")
    nstart = 1
    nend = 20
    nstep = 1
    nbParties = 100000
    heuristiqueJ2 = simulation.StrategieAleatoire
    print("\tCalcul du graphe en cours...")
    print("\tQuand le graphique s'affichera, fermer la fenêtre pour continuer ce programme de démonstration.")
    print("\tOn observe que J1 gagne relativement plus souvent avec les règles modifiées qu'avec les règles initiales.")
    plot.plotOptiContreHeuristique(nstart, nend, nstep, nbParties, heuristiqueJ2, simulation.simulerPartieModifs)

    continuer()


if __name__ == "__main__":
    main()
