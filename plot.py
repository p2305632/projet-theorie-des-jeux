from numpy import Infinity
import calcul
import matplotlib.pyplot as plt
import time
import simulation

# Réalise un graphique du temps d'exécution de la fonction calculVS(n, n, -1).
# nstart est la valeur de départ de n.
# nend est la valeur finale de n.
# nstep est le pas à appliquer.
def plotTime(nstart, nend, nstep, calc = calcul.calculeVS):
    times = []
    ns = range(nstart, nend, nstep)
    for n in ns:
        start_time = time.time()
        calc(n, n, -1)
        end_time = time.time()
        times.append(end_time - start_time)
    
    plt.plot(ns, times, marker='o')
    plt.xlabel('n')
    plt.ylabel("Temps d'exécution (s)")
    plt.title("Temps d'exécution de calculeVS(n, n, -1) en fonction de n")
    plt.grid(True)
    plt.show()


def plotOptiContreHeuristique(nstart, nend, nstep, nbParties, heuristiqueJ2, simulateurPartie=simulation.simulerPartie):
    v1_v2_ratios = []
    ns = range(nstart, nend, nstep)

    for n in ns:
        v1, v2, _ = simulation.simuler(n, nbParties, heuristiqueJ2, simulateurPartie)
        if v2 > 0:
            v1_v2_ratios.append(v1 / v2)
        else:
            v1_v2_ratios.append(Infinity)

    # Tracer le graphique
    plt.plot(ns, v1_v2_ratios, marker='o', linestyle='-')
    plt.title('Ratio v1/v2 en fonction de n pour ' + str(nbParties) + ' parties jouées')
    plt.xlabel('n')
    plt.ylabel('Ratio v1/v2')
    plt.grid(True)
    plt.show()

def main():
    #plotTime(1, 100, 10)
    plotOptiContreHeuristique(1, 20, 1, 10000, simulation.StrategieAleatoire)

if __name__ == "__main__":
    main()
