import random
from unittest import result

import calcul

#import calcul

def StrategieAleatoire(x, y, t):
    array = [0] * x
    array[random.randint(0, x - 1)] = 1

    return array

def StrategieOpti(x, y, t):
    (_, strategie) = calcul.calculeVS(x, y, t)
    return strategie

# renvoie 1 si J1 a gagné, 2 si J2 a gagné, 3 si égalité, 0 si la partie n'est pas terminée
def termine(x, y, t):
    if t == -calcul.mp:
        return 2
    if t == calcul.mp:
        return 1
    
    if x == 0:
        if y > t:
            return 2
        elif y == t:
            return 3
        else: # y < t
            return 1
    elif y == 0:
        if x > -t:
            return 1
        elif x == -t:
            return 3
        else: # x < -t
            return 2
    
    return 0

# retourne 1 pour une victoire de J1, 2 pour une victoire de J2, 0 pour un match nul
def simulerPartie(strategieJ1, strategieJ2, n):
    x = n
    y = n
    t = 0

    jeuFini = 0

    while jeuFini == 0:
        pierresJ1 = random.choices(range(1, x+1), strategieJ1(x, y, t))[0]
        # On inverse les variables et t car la fonction considère qu'elle est le joueur 1
        pierresJ2 = random.choices(range(1, y+1), strategieJ2(y, x, -t))[0]

        x = x - pierresJ1
        y = y - pierresJ2
        if pierresJ1 > pierresJ2:
            t = t + 1
        elif pierresJ1 < pierresJ2:
            t = t - 1
        
        jeuFini = termine(x, y, t)
    
    if jeuFini == 3: # on convertit les valeurs pour un match nul
        return 0
    else:
        return jeuFini

# retourne 1 pour une victoire de J1, 2 pour une victoire de J2, 0 pour un match nul
# Utilise les règles du jeu modifiées
def simulerPartieModifs(strategieJ1, strategieJ2, n):
    x = n
    y = n
    t = 0

    jeuFini = 0

    while jeuFini == 0:
        pierresJ1 = random.choices(range(1, x+1), strategieJ1(x, y, t))[0]
        # Le random se fait sur les pierres impaires
        choixJ2 = random.choices(range(0, y // 2 + y % 2), strategieJ2(y // 2 + y % 2, x, -t))[0]
        pierresJ2 = choixJ2 * 2 + 1

        x = x - pierresJ1
        y = y - pierresJ2
        if pierresJ1 > pierresJ2:
            t = t + 1
        elif pierresJ1 < pierresJ2:
            t = t - 1
        
        jeuFini = termine(x, y, t)
    
    if jeuFini == 3: # on convertit les valeurs pour un match nul
        return 0
    else:
        return jeuFini
    
# Simule nbParties parties entre la stratégie optimale pour le joueur 1 et une heuristique pour le joueur 2.
# Les joueurs commencent chacun avec n pierres à cahque partie.
# Retourne (nbVictoiresJ1, nbVictoireJ2, nbNulles)
def simuler(n, nbParties, heuristiqueJ2 = StrategieAleatoire, simulateurPartie=simulerPartie):
    v1 = 0
    v2 = 0
    nulle = 0

    for i in range(0, nbParties):
        res = simulateurPartie(StrategieOpti, heuristiqueJ2, n)

        if res == 0:
            nulle += 1
        elif res == 1:
            v1 += 1
        elif res == 2:
            v2 += 1
    
    return (v1, v2, nulle)


if __name__ == "__main__":
    nbParties = 10000
    n = 10
    v1, v2, nulle = simuler(n, nbParties, StrategieAleatoire)
    
    print(f"Sur {nbParties} parties, {v1} victoires du joueur 1, {v2} victoires du joueur 2, et {nulle} nulles. J1 a gagné {v1 / v2} fois plus que J2.")