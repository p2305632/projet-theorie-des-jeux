from decimal import Clamped
from operator import ne
from StrategieOptimale import get_nash
import numpy as np

# mp est le nombre de case dans chaque camp.
# -mp est le numéro de la case du chateau du joueur 1.
# mp est le numéro de la case du chateau du joueur 2.
mp = 2
# m est le nombre total de cases (en prenant en compte la case centrale et les deux chateaux).
m = 2 * mp + 1
# n est le nombre de pierres de départ de chaque joueur.
n = 20

# Pour afficher une configuration de jeu.
def afficherConfig(c):
    (pierresJ1, pierresJ2, posTrollRel) = c
    posTroll = mp + posTrollRel - 1

    if posTrollRel == -mp:
        print("[", pierresJ1, " pierres] J1 PERDU|", end='')
    else:
        print("[", pierresJ1, " pierres] J1|", end='')

    for i in range(0, m - 2):
        if i == posTroll:
            print("T|", end='')
        else:
            print(" |", end='')

    if posTrollRel == mp:
        print("PERDU J2 [", pierresJ2, " pierres]")
    else:
        print("J2 [", pierresJ2, " pierres]")

# Cette fonction est récursive et donne la valeur d'un jeu, ainsi que la solution optimale du joueur 1.
def calculeVS(x, y, t, memo={}):
    # On gagne du temps lorsqu'une valeur a déjà été trouvée.
    if (x, y, t) in memo:
        return memo[(x, y, t)]
    
    # Cas d'arrêt de la récurrence

    # Ici on traite en priorité la défaite d'un des deux joueurs (quand le troll est sur un chateau).
    if t <= -mp:
        return -1, np.zeros(x)
    elif t >= mp:
        return 1, np.zeros(x)

    # Ici des cas d'arrêt plus particuliers : quand un joueur n'a plus de pierre.
    if x == 0: # J1 n'a plus de pierre.
        # Ceci veut dire que J2 peut lancer ses pierres une par une (1 est impair), et faire passer le troll de l'autre côté du terrain. Donc J2 est vainqueur.
        if y > t:
            return -1, np.zeros(x)
        # Ceci veut dire que J2 a autant de pierres que le troll a avancé de cases chez lui. Il peut donc faire reculer le troll jusqu'au centre du terrain. Donc il y a match nul, le troll est pile au centre.
        elif y == t:
            return 0, np.zeros(x)
        # Ceci veut dire que le troll est avancé dans le camp de J2 de plus de cases que J2 n'a de pierres. J2 ne pourra donc pas repousser le troll hors de son camp. Et donc J1 est vainqueur.
        else: # y < t
            return 1, np.zeros(x)
    # On a le cas parfaitement symmétrique. On traite cependant avec -t, car une valeur de t négative implique que le troll se trouve dans le camp de J1.
    elif y == 0:
        if x > -t:
            return 1, np.zeros(x)
        elif x == -t:
            return 0, np.zeros(x)
        else: # x < -t
            return -1, np.zeros(x)
    elif x == y and t == 0:
        sol = [1.0 / float(x)] * x
        return 0, sol

    # Ici, le nombre de coups possibles pour J2 ne correspond plus à son nombre de pierres.
    # coupsJ2 correpsond au nombre de nombres impairs entre 1 et y.
    coupsJ2 = y // 2 + y % 2

    # Construction de la matrice des gains du jeu.
    # Le nombre de colonnes est donc divisé par 2 (à 1 près) car J2 ne peut lancer qu'un nombre impair de pierres.
    J = np.zeros((x, coupsJ2))
    for i in range(0, x):
        for j in range(0, coupsJ2):
            # On rajoute la règle pour que le troll puisse avancer de deux cases car un joueur lance au moins 3 pierres de plus que son adversaire.
            if j > i + 2:
                new_t = t - 2
            elif i > j + 2:
                new_t = t + 2
            elif j > i: # Dans le triangle inférieur, on a t - 1
                new_t = t - 1
            elif i > j: # Dans le triangle supérieur, on a t + 1
                new_t = t + 1
            else: # Sur la diagonale, on a t.
                new_t = t
            
            # On vérifie que le troll n'ait pas sauté par dessus un chateau... En cas d'avancée de 2 cases
            if new_t > mp:
                new_t = mp
            if new_t < -mp:
                new_t = -mp
            
            # Le nombre de pierres de J2 (y) diminue de 2j + 1 car j correspond à 2j + 1 pierres lancées par J2
            # Le nombre de pierres de J1 (x) diminue de x + 1 car x correspond à x + 1 pierres lancées par J1
            J[i][j], _ = calculeVS(x - 1 - i, y - 1 - j * 2, new_t)

    # On récupère la valeur de ce jeu, et la stratégie optimale pour J1.
    value, strategy = get_nash(J)

    # Et on sauvegarde cela pour les prochains passages dans la fonction.
    memo[(x, y, t)] = (value, strategy)
    return value, strategy

def main():
    # L'exemple du sujet, pour le jeu (5, 4, -1).
    pierresJ1 = 5
    pierresJ2 = 4
    positionTroll = -1
    valeur, strategie = calculeVS(pierresJ1, pierresJ2, positionTroll)
    print(f"V({pierresJ1}, {pierresJ2}, {positionTroll}) =", valeur)
    print(f"Sopt({pierresJ1}, {pierresJ2}, {positionTroll}) =", strategie)

    afficherConfig((pierresJ1, pierresJ2, positionTroll))
    

if __name__ == "__main__":
    main()
